﻿using Android.App;
using Android.Graphics.Drawables;
using Android.Widget;
using Android.OS;
using Android.Provider;
using SALLab02;

namespace ReAl.Xamarin.Lab02.Android
{
    [Activity(Label = "ReAl.Xamarin.Lab02.Android", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Validate();
        }

        private async void Validate()
        {
            var client = new SALLab02.ServiceClient();
            var miCorreo = "xxxx@gmail.com";
            var miPass = "PASSWORD";
            var miDeviceId = Settings.Secure.GetString(ContentResolver, Settings.Secure.AndroidId);

            SALLab02.ResultInfo resultado = await client.ValidateAsync(miCorreo, miPass, miDeviceId);

            AlertDialog.Builder miDialogo = new AlertDialog.Builder(this);
            AlertDialog miAlert = miDialogo.Create();
            miAlert.SetTitle("Resultado");
            miAlert.SetIcon(Resource.Drawable.Icon);
            miAlert.SetMessage(resultado.Status + "\n" + resultado.Fullname + "\n" + resultado.Token);
            miAlert.SetButton("OK", (sender, args) => {});
            miAlert.Show();
        }
    }
}

